# Supplied by CI, contains URL to dependency proxy
ARG CREGISTRY_URL
ARG FLAVOR

FROM ${CREGISTRY_URL}/ict/images/alpine/base:${FLAVOR}

# fuse-overlayfs is implementation of overlay+shiftfs in FUSE for rootless containers
RUN apk add --no-cache ca-certificates curl buildah fuse-overlayfs netavark

# Set buildah env variable
ENV BUILDAH_ISOLATION=chroot

# set container startup script
COPY --chmod=775 entrypoint.sh /
ENTRYPOINT /entrypoint.sh
