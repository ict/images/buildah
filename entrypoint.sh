#!/bin/sh

echo -e "\e[1;32mLogging into GitLab CI registry\e[m"
buildah login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

echo -e "\e[1;32mBuildah info:\e[m"
buildah info

/bin/sh "$@"
